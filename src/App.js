import React from 'react';
import logo from './logo.svg';
import './App.css';
import PathfindingVisualization from './PathfindingVisualization.js';

function App() {
  return (
    <div className="App">
      <PathfindingVisualization cols={65} rows={65}></PathfindingVisualization>
    </div>
  );
}

export default App;
