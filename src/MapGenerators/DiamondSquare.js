var gen = require('random-seed');

class DiamondSquare{
 
    constructor(heightScale, size, randomness = 2, randomSeed = "Felix", smoothFilterSize = 0){
        this.heightScale = heightScale;

        // Can only generate sizes of 2^n + 1
        let generateSize = 2;
        while(generateSize + 1 < size)
            generateSize *= 2;
        this.size = generateSize + 1;
        this.randomness = randomness;
        this.randomSeed = randomSeed;
        this.smoothFilterSize = smoothFilterSize;
    }

    applyToNodes(nodes){
        let heightValues = this.generate();
        nodes.map((node) => {
            node.height = heightValues[node.col][node.row] * this.heightScale;
        });
    }

    generate(){
        this.setRandomSeed(this.randomSeed);
        let size = this.size;
        // Initialize map with zeroes
        let map = [];
        for(let x = 0; x < size; ++x){
            map[x] = [];
            for(let y = 0; y < size; ++y){
                map[x][y] = 0;
            }
        }

        // Set random corner points
        map[0][0] = this.gaussianRandom();
        map[0][size - 1] = this.gaussianRandom();
        map[size - 1][0] = this.gaussianRandom();
        map[size - 1][size - 1] = this.gaussianRandom();

        let stepSize = size - 1;
        while(stepSize > 1){
            this.squareStep(map, stepSize, size);
            this.diamondStep(map, stepSize, size);
            stepSize /= 2;
        }
        return this.smoothFilterSize > 0 ? this.smooth(map, this.smoothFilterSize) : map;
    }

    squareStep(map, stepSize, size){
        let halfStepSize = stepSize / 2;
        for(let x = 0; x < size - 1; x += stepSize){
            for(let y = 0; y < size - 1; y += stepSize){
                map[x + halfStepSize][y + halfStepSize] = this.clamp((map[x][y] + map[x + stepSize][y] + map[x][y + stepSize] + map[x + stepSize][y + stepSize]) / 4.0 + this.randomnessValue(stepSize), 0, 1);
            }
        }
    }

    diamondStep(map, stepSize, size){
        let halfStepSize = stepSize / 2;
        let evenRow = true;
        for(let y = 0; y < size; y += halfStepSize){
            for(let x = evenRow ? halfStepSize : 0; x < size; x += stepSize){
                let sum = 0;
                let n = 0;
                [[x - halfStepSize, y], 
                 [x, y - halfStepSize], 
                 [x + halfStepSize, y],
                 [x, y + halfStepSize]
                ].map(([x, y]) => {
                    if(x >= 0 && x < size && y >= 0 && y < size){
                        sum += map[x][y];
                        n++;
                    }
                });
                map[x][y] = this.clamp(sum / n + this.randomnessValue(stepSize), 0, 1);
            }
            evenRow = !evenRow;
        }
    }

    randomnessValue(stepSize){
        let halfStepSize = stepSize / 2;
        let sign = (this.rand.random() > 0.5 ? 1 : -1);
        return sign * this.randomness * this.gaussianRandom() * (stepSize / this.size);
    }

    clamp(value, min, max){
        return Math.max(min, Math.min(max, value));
    }

    smooth(map, filterSize){
        let width = map.length;
        let height = map[0].length;
        let smoothMap = [];
        for(let i = 0; i < this.size; ++i){
            smoothMap[i] = [];
        }
        let filterHalf = Math.floor(filterSize / 2);
        for(let y = 0; y < this.size; ++y){
            for(let x = 0; x < this.size; ++x){
                
                let xLower = x - filterHalf > 0 ? x - filterHalf : 0;
                let xUpper = x + filterHalf < width ? x + filterHalf : width - 1;
                let yLower = y - filterHalf > 0 ? y - filterHalf : 0;
                let yUpper = y + filterHalf < height ? y + filterHalf : height - 1;

                let sum = 0;
                let n = 0;
                for(let fy = yLower; fy <= yUpper; ++fy){
                    for(let fx = xLower; fx <= xUpper; ++fx){
                        sum += map[fx][fy];
                        ++n;
                    }
                }
                smoothMap[x][y] = sum / n;
            }
        }
        return smoothMap;
    }

    gaussianRandom() {
        var random = 0;
        
        for (var i = 0; i < 6; i += 1) {
            random += this.rand.random();
        }
        
        return random / 6;
    }

    setFilterSize(filterSize){
        this.smoothFilterSize = filterSize;
    }

    setRandomness(randomness){
        this.randomness = randomness;
    }

    setRandomSeed(randomSeed){
        this.randomSeed = randomSeed;
        this.rand = gen.create(randomSeed);
    }

    setHeightDisplacement(displacement){
        this.heightDisplacement = displacement;
    }
    
}

export default DiamondSquare;