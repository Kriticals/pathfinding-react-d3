import React from 'react';
import { SmileOutlined } from '@ant-design/icons';
import { Radio, Typography , Input, Switch, Collapse, Slider } from 'antd';

const { Text } = Typography;
const { Panel } = Collapse;
const { Search } = Input;

class DiamondSquareSettings extends React.Component{

    constructor(props){
        super(props);
    }

    onCollapseChange(key){

    }

    render(){
        const { enabled, seed, smoothingSize, randomnessFactor, onRandomSeed, onToggle, onSmoothingSize, onRandomness, onHeightDisplacement } = this.props;
        return (
            
                <Collapse defaultActiveKey={['1']} onChange={this.onCollapseChange}>
                    <Panel header={<Text>Heightmap</Text>} key="1">
                    <div className="settings-group">
                        <div>
                            <Text>Enabled</Text><br />
                            <Switch
                                style={{marginTop: 10}}
                                defaultChecked
                                onChange={onToggle} 
                            />
                        </div>
                        <div>
                            <Text>Seed</Text>
                            <Search 
                                style={{marginTop: 10}}
                                disabled={!enabled}
                                placeholder="Enter seed or generate!"
                                enterButton="Generate"
                                size="medium"
                                onSearch={ () => { onRandomSeed(null) } }
                                onChange={(e) => onRandomSeed(e.target.value)}
                                value={seed}
                            />
                        </div>
                        <div>
                            <Text>Smoothing</Text>
                            <Radio.Group 
                                style={{marginTop: 10}} 
                                disabled={!enabled} 
                                value={smoothingSize} 
                                onChange={(e)=>onSmoothingSize(e.target.value)}
                            >
                                <Radio.Button value={0}>Off</Radio.Button>
                                <Radio.Button value={3}>3x3</Radio.Button>
                                <Radio.Button value={5}>5x5</Radio.Button>
                                <Radio.Button value={7}>7x7</Radio.Button>
                            </Radio.Group>
                        </div>
                        <div>
                            <Text>Offset Factor</Text><br />
                            <Slider
                                min={1}
                                max={15}
                                step={0.01}
                                defaultValue={randomnessFactor} 
                                onChange={ onRandomness } 
                            />
                        </div>
                    </div>
                    </Panel>
                </Collapse>
        )
    }

}

export default DiamondSquareSettings;