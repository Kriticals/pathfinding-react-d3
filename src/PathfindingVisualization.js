import React from 'react';
import * as d3 from 'd3';
import AlgorithmSettings from './Algorithms/AlgorithmSettings.js';
import DiamondSquare from './MapGenerators/DiamondSquare.js';
import DiamondSquareSettings from './MapGenerators/DiamondSquareSettings.js';
import NodeType from './NodeType.js'
import AStar from './Algorithms/AStar.js';
import BreadthFirst from './Algorithms/BreadthFirst.js';
import VisualizationControls from './VisualizationControls.js';

import { Layout, Row, Col, Menu, Button } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';

const { Header, Sider, Content } = Layout;
class PathfindingVisualization extends React.Component{
    
    constructor(props){
        super(props);

        this.state = {
            siderCollapsed: false,
            running: false,
            seed: this.props.seed ? this.props.seed : "0.020225846231847422",
            animationTimeout: 25,
            diamondSquareEnabled: true,
            diamondSquareSmoothing: 3,
            diamondSquareRandomnessFactor: 2.5,
        }
    }

    toggleSider = () => {
        this.setState({
            siderCollapsed: !this.state.siderCollapsed,
        });
    };

    componentDidMount(){
        this.initializeMap();
        this.initializePathfinder(this.pathfindingAlgorithm);
        this.runInterval = null;        
    }

    createGridNode(row, col, type){
        return {
            col: col, 
            row: row, 
            index: this.getIndexFromCoordinates(row, col),
            type: type,
            height: 0,
        };
    }

    initializePathfinder(algorithm){
        this.pathfindingAlgorithm = algorithm;

        if(this.nodes){
            this.pathfinder = new this.pathfindingAlgorithm(this.nodes, this.start, this.end, this.props.rows, this.props.cols);
            this.updateMap();
        }
    }

    initializeMap(){
        let that = this;

        this.nodes = [];
        for(let row = 0; row < this.props.rows; row++){
            for(let col = 0; col < this.props.cols; col++){
                this.nodes.push(this.createGridNode(row, col, NodeType.DEFAULT));
            }
        }
        
        this.diamondSquare = new DiamondSquare(this.props.rows, Math.max(this.props.rows, this.props.cols), this.state.diamondSquareRandomnessFactor, this.state.seed, this.state.diamondSquareSmoothing);
        if(this.state.diamondSquareEnabled){
            this.diamondSquare.applyToNodes(this.nodes);
        }

        let mouseDown = false;
        let width = 1000 / this.props.cols;
        let height = width;

        this.grid = d3.select('#grid')
        .append('svg')
        .attr("viewBox", `0 0 1000 1000`)
        .append('g')
        .style('fill', '#fff');

        this.column = this.grid.selectAll(".square")
        .data(this.nodes)
        .enter().append("rect")
        .attr("class","square")
        .attr("x", function(d) { return d.col * width; })
        .attr("y", function(d) { return d.row * height; })
        .attr("width", width)
        .attr("height", height)
        .style("opacity", function(d){
            switch(d.type){
                case NodeType.DEFAULT:
                    return (1 - ((d.height / that.props.cols) * 0.8 + 0.1)) ;
                case NodeType.OBSTACLE:
                    return 1;
                default:
                    return 0.75 + (1 - (d.height / that.props.cols)*0.8) / 4;
            }
        })
        .on('mousedown', function(d){
            if(that.state.running)
                return;

            mouseDown = true;
            switch(d3.event.button){
                case 0:
                    that.toggleObstacle(d.index);
                    break;
                case 1:
                    that.setStart(d.index);
                    break;
                case 2:
                    that.setEnd(d.index);
                    break;
            }
        })
        .on('mouseup', function(){
            mouseDown = false;
        })
        .on('mouseenter', function(d) {
            if(!mouseDown)
                return;
            
            if(d3.event.button === 0){
                that.toggleObstacle(d.index);
            }
        })
        .on('contextmenu', function(){
            d3.event.preventDefault();
        });

        this.setStart(this.props.cols * 10 + 10);
        this.setEnd(this.props.cols * (this.props.rows - 10) - 10)
    }



    updateMap(){
        let that = this;

        this.column
        .attr("class", function(d){
            return "square " + d.type;
        })
        .style("opacity", function(d){
            switch(d.type){
                case NodeType.DEFAULT:
                    return (1 - ((d.height / that.props.cols) * 0.8 + 0.1)) ;
                default:
                    return 0.25 + (1 - (d.height / that.props.cols)*0.8) * 0.75;
            }
        })
    }

    async animatePath(immediately = false, recalculate = false){
        if(recalculate){
            this.initializePathfinder(this.pathfindingAlgorithm);
        }
        let path = this.pathfinder.getPath();
        if(this.pathfinder.finished()){
            for(let index of path){
                if(!this.state.running)
                    return;

                if(index === this.start || index === this.end)
                    continue;

                this.nodes[index].type = NodeType.PATH;
                if(!immediately){
                    this.updateMap();
                    await sleep(this.state.animationTimeout);
                }
            }
            this.nodes.map((node)=>{
                if(node.type === NodeType.QUEUED || node.type === NodeType.VISITED){
                    node.type = NodeType.DEFAULT;
                }
            });
            this.updateMap();
        }
        this.setState({running: false});
    }

    async startRun(){
        await this.promisedSetState({running: true});
        this.initializePathfinder(this.pathfindingAlgorithm);
        while(this.state.running && !this.pathfinder.finished()){
            this.pathfinder.findPathStep();
            this.updateMap();
            await sleep(this.state.animationTimeout);
        }
        this.animatePath();
    }

    stopRun(){
        this.setState({running: false});
    }

    setStart(index){
        this.stopRun();
        if(this.start !== undefined){
            this.nodes[this.start].type = NodeType.DEFAULT;
        }
        this.nodes[index].type = NodeType.START;
        this.start = index;

        if(this.pathfinder)
            this.initializePathfinder(this.pathfindingAlgorithm);

        this.updateMap();
    }

    setEnd(index){
        this.stopRun();
        if(this.end !== undefined){
            this.nodes[this.end].type = NodeType.DEFAULT;
        }
        this.nodes[index].type = NodeType.END;
        this.end = index;

        if(this.pathfinder)
            this.initializePathfinder(this.pathfindingAlgorithm);

        this.updateMap();
    }

    toggleObstacle(index){
        if(index === this.start || index === this.end)
            return;

        let node = this.nodes[index];

        if(node.type === NodeType.OBSTACLE){
            node.type = NodeType.DEFAULT;
        } else {
            node.type = NodeType.OBSTACLE;
        }
        this.updateMap();
    }
    
    getIndexFromCoordinates(row, col){
        return (row * this.props.cols + col);
    }
    
    getCoordinatesFromIndex(index){
        let row = Math.floor(index / this.props.cols);
        let col = index % this.props.cols;
        return {row: row, col: col};
    }

    useHeightmap(value){
        if(value){
            this.diamondSquare.applyToNodes(this.nodes);
            this.initializePathfinder(this.pathfindingAlgorithm);
            this.updateMap();
        } else {
            this.nodes.map((node) => {
                node.height = this.props.rows;
            });
            this.initializePathfinder(this.pathfindingAlgorithm);
            this.updateMap();
        }
    }

    generateRandomMap(seed = null){
        if(seed === null)
            seed = Math.random();

        this.setState({seed: seed}, () => {
            this.diamondSquare.setRandomSeed(seed); 
            this.diamondSquare.applyToNodes(this.nodes);
            this.initializePathfinder(this.pathfindingAlgorithm);
            this.updateMap();
        })
    }

    setMapSmoothingSize(smoothingSize){
        console.log(smoothingSize);
        this.setState({diamondSquareSmoothing: smoothingSize}, ()=>{
            this.diamondSquare.setFilterSize(smoothingSize);
            this.diamondSquare.applyToNodes(this.nodes);
            this.initializePathfinder(this.pathfindingAlgorithm);
            this.updateMap();
        })
    }

    setMapRandomnessFactor(factor){
        this.setState({diamondSquareRandomness: factor}, () => {
            this.diamondSquare.setRandomness(factor);
            this.diamondSquare.applyToNodes(this.nodes);
            this.initializePathfinder(this.pathfindingAlgorithm);
            this.updateMap();
        })
    }

    render(){
        const { running, seed, diamondSquareSmoothing, diamondSquareRandomnessFactor, diamondSquareEnabled } = this.state;
        return (
            <>
            <Layout>
                <Sider collapsedWidth={0} collapsed={this.state.siderCollapsed} breakpoint='md' theme='light' className="Sider" width={'300px'} trigger={null}>
                    <div className="logo" />
                    
                    <AlgorithmSettings 
                        onAlgorithmChanged={this.initializePathfinder.bind(this)}
                        onAnimationTimeoutChange={(value) => this.setState({animationTimeout: value})}
                    />
                    <DiamondSquareSettings 
                        enabled={diamondSquareEnabled}
                        seed={seed} 
                        smoothingSize={diamondSquareSmoothing}
                        randomnessFactor={diamondSquareRandomnessFactor}
                        onToggle={(enabled) => this.useHeightmap(enabled)}
                        onRandomSeed={(seed)=>this.generateRandomMap(seed)}
                        onRandomness={(value)=>this.setMapRandomnessFactor(value)}
                        onSmoothingSize={(smoothingSize) => this.setMapSmoothingSize(smoothingSize)}
                    />
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 10, height: 'auto', lineHeight: 'auto'}}>
                        <Row>
                            <Col>
                                <Button style={{margin: '10px'}} onClick={this.toggleSider} icon={<MenuFoldOutlined />} />
                            </Col>
                            <Col></Col>
                        <VisualizationControls 
                            running={this.state.running} 
                            onStartStop={() => {running ? this.stopRun() : this.startRun()}}
                            onShowResult={() => this.setState({running: true}, this.animatePath.bind(this, true, true))}
                        />
                        </Row>
                        
                    </Header>
                    <Content
                        className="site-layout-background"
                        style={{
                        margin: '24px 16px',
                        minHeight: 280,
                        }}
                    >
                        <div id="grid"></div>
                    </Content>
                </Layout>
            </Layout>
            </>
        );
    }

    promisedSetState = (newState) => {
        return new Promise((resolve) => {
            this.setState(newState, () => {
                resolve()
            });
        });
    }    

}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


export default PathfindingVisualization;