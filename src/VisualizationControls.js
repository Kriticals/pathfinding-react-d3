import React from 'react';
import { Button, Typography } from 'antd';
import { PlayCircleFilled, SyncOutlined, DashboardOutlined } from '@ant-design/icons';

const { Text } = Typography;

function VisualizationControls(props){
    const { running, onStartStop, onShowResult } = props;
    return(<>
    <div className="settings-group-inline">
        <div>
            <Button type="primary" shape="round" icon={ running ? <SyncOutlined /> : <PlayCircleFilled />} size="large" onClick={ onStartStop }>
                {running ? 'Stop' : 'Start'}
            </Button>
        </div>
        <div>
            <Button type="primary" icon={ <SyncOutlined /> } size="large" onClick={ onShowResult }>
                Show Result
            </Button>
        </div>        
    </div>
        
    </>);
    
}

export default VisualizationControls;