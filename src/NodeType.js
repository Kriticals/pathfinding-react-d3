const NodeType = {
    DEFAULT: "default",
    START: "start", 
    END: "end",
    OBSTACLE: "obstacle",
    VISITED: "visited", 
    QUEUED: "queued",
    PATH: "path"
}

export default NodeType;