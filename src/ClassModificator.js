import React from 'react';

class ClassModificator extends React.Component{

  constructor(props){
    super(props);
  }

  render(){
    const { someClass } = this.props;

    return(
      <span>{someClass.getA()}</span>
    );
  }

}