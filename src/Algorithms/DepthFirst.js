import AbstractPathfinder from "./AbstractPathfinder.js"
import NodeType from "../NodeType.js";

class DepthFirst extends AbstractPathfinder{

    constructor(nodes, start, end, rows, cols){
        super(nodes, start, end, rows, cols);

        nodes.forEach(node => {
            node.visited = false;
            node.isQueued = false;
            this.cost[node.index] = Number.MAX_VALUE;
        });

        this.finishedRun = false;
        this.cost[start] = 0;
        this.queue.push(start);
    }

    findPathStep(){
        if(this.queue.length > 0){
            let index = this.getNextNode(false);
            if(index == this.end){
                this.finishedRun = true;
                this.queue = []
                return;
            }

            let neighbors = this.getNeighbors(index);
            neighbors.forEach(i => {
                let neighbor = this.nodes[i];
                if(neighbor.type === NodeType.OBSTACLE)
                    return;
    
                if(neighbor.isQueued || neighbor.visited)
                    return;

                this.cost[i] = this.cost[index] + 1;
                this.predecessor[i] = index;
                this.addToQueue(i);
            });
            this.setVisited(index);
        } else {
            this.finishedRun = true;
        }
    }
    
    finished(){
        return this.finishedRun;
    }

}

export default DepthFirst;