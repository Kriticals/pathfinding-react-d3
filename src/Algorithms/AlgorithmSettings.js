import React from 'react'
import { Select, Typography, Slider } from 'antd';
import AStar from './AStar.js';
import BreadthFirst from './BreadthFirst.js';
import DepthFirst from './DepthFirst.js';

const { Text } = Typography;
const { Option } = Select;

const minTimeout = 5;
const maxTimeout = 500;
const defaultTimeout = 25;

const algorithms = {
    breadthFirst: {
        class: BreadthFirst, 
        displayName: "Breadth First",
        options: {

        }
    },
    depthFirst: {
        class: DepthFirst, 
        displayName: "Depth First"
    },
    aStar: {
        class: AStar, 
        displayName: "A*"
    }
};

class AlgorithmSettings extends React.Component{

    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.props.onAlgorithmChanged(algorithms.aStar.class);
    }

    render(){
        const {onAlgorithmChanged, onAnimationTimeoutChange} = this.props;

        return (
            <div className="settings-group">
                <div>
                    <Text>Algorithm</Text><br />
                    <Select 
                        defaultValue={"aStar"} 
                        style={{ width: '100%', marginTop: 10 }} 
                        onChange={(value) => onAlgorithmChanged(algorithms[value].class)}
                    >
                        {Object.keys(algorithms).map((i) => 
                            <Option key={i} value={i}>{algorithms[i].displayName}</Option>
                        )}
                    </Select>
                </div>
                
                <div>
                    <Text style>Animation Speed</Text><br />
                    <Slider
                        min={minTimeout}
                        max={maxTimeout}
                        defaultValue={defaultTimeout} 
                        onAfterChange={ onAnimationTimeoutChange } 
                    />
                </div>
            </div>
            
        );
        
    }    

}

export default AlgorithmSettings;