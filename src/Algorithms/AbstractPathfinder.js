import NodeType from '../NodeType.js';

class AbstractPathfinder{

    constructor(nodes, start, end, rows, cols){
        if (new.target === AbstractPathfinder) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }

        this.nodes = nodes;
        this.start = start;
        this.end = end;
        this.rows = rows;
        this.cols = cols;
        this.reset();
    }

    findPath(){
        while(!this.finished()){
            this.findPathStep();
        }
        return this.getPath();
    }

    getNeighbors(index){
        let neighbors = [];

        let col = index % this.cols;
        let row = Math.floor(index / this.cols);

        // Left Node
        if(col > 0){
            neighbors.push(index - 1);
        }

        // Right Node
        if(col < this.cols - 1){
            neighbors.push(index + 1);
        }
        
        // Top Node
        if(row > 0){
            neighbors.push(index - this.cols);
        }

        // Bottom Node
        if(row < this.rows - 1){
            neighbors.push(index + this.cols);
        }

        return neighbors;
    }

    getNextNode(front){
        let node = front ? this.queue.shift() : this.queue.pop();
        this.nodes[node].isQueued = false;
        return node;
    }

    setVisited(index){
        let node = this.nodes[index];
        node.visited = true;
        if(node.type !== NodeType.START && node.type !== NodeType.END)
            node.type = NodeType.VISITED;
        this.visited[index] = true;
    }

    addToQueue(nodeIndex){
        let node = this.nodes[nodeIndex];
        if(!node.isQueued){
            this.queue.push(nodeIndex);
            node.isQueued = true;
            if(node.type !== NodeType.START && node.type !== NodeType.END)
                node.type = NodeType.QUEUED;
        }
    }

    euclideanDistanceHeuristic(index){
        let node = this.nodes[index];
        let goalNode = this.nodes[this.end];
        let colDiff = goalNode.col - node.col;
        let rowDiff = goalNode.row - node.row;
        let heightDiff = node.height ? goalNode.height - node.height : 0;
        return Math.sqrt(colDiff * colDiff + rowDiff * rowDiff + heightDiff * heightDiff);
    }

    manhattenHeuristic(index){
        let node = this.nodes[index];
        let goalNode = this.nodes[this.end];
        let colDiff = Math.abs(goalNode.col - node.col);
        let rowDiff = Math.abs(goalNode.row - node.row);
        let heightDiff = node.height ? Math.abs(goalNode.height - node.height) : 0;
        return colDiff + rowDiff + heightDiff;
    }

    getPath(){
        if(!this.finished()){
            this.findPath();
        }
        if(this.predecessor.hasOwnProperty(this.end)){
            let path = [];
            let currentIndex = this.end;
            path.unshift(currentIndex);
            while(currentIndex !== this.start){
                currentIndex = this.predecessor[currentIndex];
                path.unshift(currentIndex);
            }
            return path;
        }   
        return [];
    }

    reset(){
        this.predecessor = []
        this.visited = []
        this.queue = []
        this.cost = []
        this.nodes[this.start].isStart = true;
        this.nodes[this.end].isEnd = true;

        this.nodes.forEach(node => {
            node.visited = false;
            node.isQueued = false;
            
            switch(node.type){
                case NodeType.VISITED:
                case NodeType.QUEUED:
                case NodeType.PATH:
                    node.type = NodeType.DEFAULT;
                break;
            }
            
        });
    }

    getNodes(){
        return this.nodes;
    }

    findPathStep(){
        throw new Error("Need to implement abstract method findPathStep.");
    }
    
    finished(){
        throw new Error("Need to implement abstract method getPath.");
    }

}

export default AbstractPathfinder;