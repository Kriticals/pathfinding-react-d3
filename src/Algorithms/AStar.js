import AbstractPathfinder from "./AbstractPathfinder.js";
import NodeType from "../NodeType.js";

class AStar extends AbstractPathfinder {

    constructor(nodes, start, end, rows, cols){
        super(nodes, start, end, rows, cols);

        this.heuristicCost = []
        this.heuristicFunction = this.manhattenHeuristic.bind(this);

        nodes.forEach(node => {
            this.cost[node.index] = Number.MAX_VALUE;
            this.heuristicCost[node.index] = this.heuristicFunction(node.index);
        });

        this.cost[start] = 0;
        this.addToQueue(start);
    }

    findPathStep(){
        if(this.queue.length > 0){
            let index = this.getNextNode(true);
            if(index == this.end){
                this.queue = []
                return;
            }
            let node = this.nodes[index];
            let neighbors = this.getNeighbors(index);
            neighbors.forEach(i => {
    
                let neighbor = this.nodes[i];
                if(neighbor.type === NodeType.OBSTACLE)
                    return;
    
                let newCost = this.cost[index] + 1 + Math.abs(node.height - neighbor.height);
                if(newCost < this.cost[i]){
                    this.cost[i] = newCost;
                    this.predecessor[i] = index;
                    this.addToQueue(i);
                }
            });
            
            this.setVisited(index);
        }
    }

    addToQueue(nodeIndex){
        super.addToQueue(nodeIndex);
        this.queue.sort(this.queueCompareFunction.bind(this));
    }

    queueCompareFunction(indexA, indexB){
        let ha = this.heuristicFunction(indexA);
        let hb = this.heuristicFunction(indexB);
        let h = ((this.cost[indexA] + ha) - (this.cost[indexB] + hb));
        return h === 0 ? ha - hb : h;
    }

    finished(){
        return this.queue.length === 0 && this.predecessor.hasOwnProperty(this.end);
    }

}

export default AStar;